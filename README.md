# Firefox update notifier

## Scrapes the Firefox package page every 10 minutes, then sends a notification if a new version is posted.

Requires `python3-bs4`. Tested on Ubuntu 18.04.

Run it in screen or tmux.
