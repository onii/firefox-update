#!/usr/bin/python3
from bs4 import BeautifulSoup
import time
import requests
import re
import os

url = 'http://packages.ubuntu.com/bionic/firefox'

def getPage():
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "lxml")
    version = soup.find('h1')
    regex = re.compile(".*?\((.*?)\)")
    result = re.findall(regex, version.contents[0].strip())
    return result[0]

curVer = getPage()
print("Current version: " + curVer + " (" + time.strftime("%Y-%m-%d %H:%M:%S") + ")")
time.sleep(2)

while getPage() == curVer:
    time.sleep(600)
print("New Firefox version! " + time.strftime("%Y-%m-%d %H:%M:%S ") + getPage())
os.system('notify-send "Detected a new version of Firefox."')
